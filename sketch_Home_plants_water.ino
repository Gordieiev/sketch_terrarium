#include <avr/sleep.h>
#include <OLED_I2C.h>

OLED  myOLED(SDA, SCL, 8);

extern uint8_t SmallFont[];
extern uint8_t MediumNumbers[];
extern uint8_t BigNumbers[];

// -------------------------------------------
// ==================  VARIABLES ==================
// -------------------------------------------

int Plant1, Plant2, Plant3, Plant4;
float mean_water;
float proc_water_buf;
int proc_water;
String Plant1_info, Plant2_info, Plant3_info, Plant4_info;
int delay_time = 1000; // delay time between reading the sensors
String ver = "v1.2.1";
// -------------------------------------------
// ==================  SETUP ==================
// -------------------------------------------
void setup() {

  myOLED.begin();
  myOLED.setFont(SmallFont);

  Serial.begin(9600);

  pinMode(A0, INPUT); // Pins for read sensor data
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);
  pinMode(A3, INPUT);

  pinMode(3, OUTPUT); // Pins for power
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(9, OUTPUT);

  set_sleep_mode (SLEEP_MODE_PWR_DOWN);
  sleep_enable();
  sleep_cpu (); 
 
}


// -------------------------------------------
// ==================  LOOP ==================
// -------------------------------------------
void loop() {

digitalWrite(3, HIGH);
delay(100); // we need to stop for a while, becouse analog pin need time for start working
Plant1 = analogRead(A0);
Serial.println(Plant1);

digitalWrite(5, HIGH);
delay(100); // we need to stop for a while, becouse analog pin need time for start working
Plant2 = analogRead(A1);
Serial.println(Plant2);

digitalWrite(6, HIGH);
delay(100); // we need to stop for a while, becouse analog pin need time for start working
Plant3 = analogRead(A2);
Serial.println(Plant3);

digitalWrite(9, HIGH);
delay(100); // we need to stop for a while, becouse analog pin need time for start working
Plant4 = analogRead(A3);
Serial.println(Plant4);


digitalWrite(3, LOW);
digitalWrite(5, LOW);
digitalWrite(6, LOW);
digitalWrite(9, LOW);


// ---------------------

mean_water = (Plant1 + Plant2 + Plant3 + Plant4)/4; // mean of water saturation
proc_water_buf = round (( (1024-mean_water) / 1024)*100); // procent of water saturation
proc_water = proc_water_buf; // Take fixed part of result
Serial.println(proc_water);

Plant1_info = Plants_info(Plant1,"1"); // function that make an String information about plant water level
Plant2_info = Plants_info(Plant2,"2");
Plant3_info = Plants_info(Plant3,"3");
Plant4_info = Plants_info(Plant4,"4");

myOLED.clrScr();

myOLED.print({"Water level " + ver}, CENTER, 0);
myOLED.print(Plant1_info, LEFT, 16); 
myOLED.print(Plant2_info, LEFT, 26);
myOLED.print(Plant3_info, LEFT, 36);
myOLED.print(Plant4_info, LEFT, 46);
myOLED.print({"Water saturation: "+ String(proc_water, DEC)+"%"}, LEFT, 56);
myOLED.update();

delay(delay_time);

}




// -------------------------------------------
// ==================  FUNCTIONS ==================
// -------------------------------------------

String Plants_info(int Plant_level, String Plant_num){ // plant water level function
String info;

if (Plant_level > 1 && Plant_level < 500){
  info = {"Plant "+Plant_num+" : ~~~~~~~"};
}
else if (Plant_level >= 500 && Plant_level < 550){
  info = {"Plant "+Plant_num+" : ~~~~~~"};
}
else if (Plant_level >= 550 && Plant_level < 600){
  info = {"Plant "+Plant_num+" : ~~~~~"};
}
else if (Plant_level >= 600 && Plant_level < 650){
  info = {"Plant "+Plant_num+" : ~~~~"};
}
else if (Plant_level >= 650 && Plant_level < 700){
  info = {"Plant "+Plant_num+" : ~~~"};
}
else if (Plant_level >= 700 && Plant_level < 750){
  info = {"Plant "+Plant_num+" : ~~"};
}
else if (Plant_level >= 750 && Plant_level < 800){
  info = {"Plant "+Plant_num+" : ~"};
}
else if (Plant_level >= 800){
  info = {"Plant "+Plant_num+" : need water!!!"};
}
else if (Plant_level <= 1){
  info = {"Sensor "+Plant_num+" isn't work!"};  
}


  return info;
}
